/**
 * @author Rodrigo Zea 17058
 * @author Franciso Molina 17050
 * @author Rodrigo Urrutia 16139
 * @version 01.06.2018
 */

package ht11;

/**
 * https://github.com/kennyledet/Algorithm-Implementations/blob/master/Floyd_Warshall/Java/dalleng/FloydWarshall.java
 * The Floyd-Warshall algorithm is used to find the shortest path between
 * all pairs of nodes in a weighted graph with either positive or negative
 * edge weights but without negative edge cycles.
 * 
 * The running time of the algorithm is O(n^3), being n the number of nodes in
 * the graph.
 *
 * This implementation is self contained and has no external dependencies. It 
 * does not try to be a model of good Java OOP, but a simple self contained 
 * implementation of the algorithm.
 */

import java.util.ArrayList;
import java.util.Arrays;

public class FloydWarshall {

    // graph represented by an adjacency matrix
    private double[][] graph;
    private String[][] recorrido;
    private boolean negativeCycle;

    public FloydWarshall() {

    }
    
    public void crearMatriz(int n) {
        this.graph = new double[n][n];
        this.recorrido = new String[n][n];
        initGraph();
        initRecorrido();
        System.out.println("Matriz creada de tamano "+graph.length);
    }

    private void initGraph() {
        for (int i = 0; i < graph.length; i++) {
            for (int j = 0; j < graph.length; j++) {
                if (i == j) {
                    graph[i][j] = 0;
                } else {
                    graph[i][j] = Double.POSITIVE_INFINITY;
                }
            }
        }
    }
    
    //se llena la matriz recorrido
    private void initRecorrido() {
        for (int i = 0; i < recorrido.length; i++) {
            for (int j = 0; j < recorrido.length; j++) {
                if (i == j) {
                    recorrido[i][j] = "0";
                } else {
                    recorrido[i][j] = "infty";
                }
            }
        }
    }

    public boolean hasNegativeCycle() {
        return this.negativeCycle;
    }

    // utility function to add edges to the adjacencyList
    public void addEdge(int from, int to, double weight) {
        graph[from][to] = weight;
    }
    
    public void imprimirGrafo() {
        for (double[] graph1 : graph) {
            for (int j = 0; j < graph1.length; j++) {
                System.out.print(graph1[j] + " ");
            }
            System.out.println();
        }
    }
    
    public void imprimirRecorrido() {
        for (int i = 0; i < recorrido.length; i++) {
            for (int j = 0; j < recorrido[i].length; j++) {
                System.out.print(recorrido[i][j] + " ");
            }
            System.out.println();
        }
    }

    // all-pairs shortest path
    public double[][] floydWarshall() {
        double[][] distances;
        
        int n = this.graph.length;        
        distances = Arrays.copyOf(this.graph, n);

        for (int k = 0; k < n; k++) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    distances[i][j] = Math.min(distances[i][j], distances[i][k] + distances[k][j]);
                    
                    //LAS SIGUIENTES TRES LINEAS DE CODIGO SON LAS IMPORTANTES
                    if (distances[i][j]>=distances[i][k] + distances[k][j]){
                        recorrido[i][j]=recorrido[i][j]+k;
                    }
                    //me imagino que se puede hacer algo mejor para archivar el recorrido optimo
                    // pero no pude
                }
            }

            if (distances[k][k] < 0.0) {
                this.negativeCycle = true;
            }
        }

        return distances;
    }

    public String[][] matrizRecorrido() {
        return recorrido;
    }
    
    public double[][] matrizGrafo() {
        return graph;
    }
}