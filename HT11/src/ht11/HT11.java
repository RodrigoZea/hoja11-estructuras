package ht11;

/**
 * @author Rodrigo Zea 17058
 * @author Franciso Molina 17050
 * @author Rodrigo Urrutia 16139
 * @version 01.06.2018
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class HT11 {
    
    public static void main(String[] args) throws IOException {
        //Se declaran los valores de vectores de ArrayList para el uso
        File archivo = new File ("guategrafo.txt");
        FileReader fr1 = new FileReader(archivo);
        BufferedReader br1 = new BufferedReader(fr1);
        String linea = " ";
        Scanner scanner = new Scanner(fr1);
        FileWriter x= new FileWriter(archivo);
        BufferedWriter writer=  new BufferedWriter(x);
        Scanner entrada = new Scanner(System.in);

        // Se inicializa el grafo
        FloydWarshall f = new FloydWarshall();

        String city1,city2;
        int distancia = 0;
        
        ArrayList<String> ciudades = new ArrayList<>();
        
        //Lecutra del archivo
        while (scanner.hasNextLine()) {
            linea = scanner.nextLine();
            city1 = linea.substring(0, linea.indexOf(" "));
            linea = linea.substring(linea.indexOf(" ") + 1, linea.length());
            city2 = linea.substring(0, linea.indexOf(" "));
            
            if (ciudades.contains(city1)==false){
                ciudades.add(city1);
            }
            if (ciudades.contains(city2)==false){
                ciudades.add(city2);
            }
            
            fr1.close();
            br1.close();
        }
        // Al leer la primera vez el archivo, se asegura el tamano del mismo 
        f.crearMatriz(ciudades.size()); //Se crea la matriz de acuerdo al tamano obtenido
        
        FileReader fr2 = new FileReader(archivo);
        BufferedReader br2 = new BufferedReader(fr2);
        scanner = new Scanner(fr2);
        
        while (scanner.hasNextLine()) {
            linea = scanner.nextLine();
            city1 = linea.substring(0, linea.indexOf(" "));
            linea = linea.substring(linea.indexOf(" ") + 1, linea.length());
            city2 = linea.substring(0, linea.indexOf(" "));
            linea = linea.substring(linea.indexOf(" ") + 1, linea.length());
            distancia = Integer.parseInt(linea.substring(0, linea.length()));
            
            f.addEdge(ciudades.indexOf(city1), ciudades.indexOf(city2), distancia);
            // Sea agregan las conexiones entre ciudades
            fr2.close();
            br2.close();
        }
        
        // La matriz distances contiene las distancias minimas entre dos ciudades
        double[][] distances;
        distances = f.floydWarshall();
        
        // La matriz grafo muestra la distancia directa entre dos ciudades
        double[][] grafo;
        grafo = f.matrizGrafo();
        
        //se imprime el codigo del recorrido. Cada numero representa una ciudad
        f.imprimirRecorrido();
        
//        f.imprimirGrafo();  

        // Para conocer las ciudades disponibles
        System.out.println(ciudades.toString());
//        
//        for (double[] distance : distances) {
//            for (int j = 0; j < distance.length; j++) {
//                System.out.print(distance[j] + " ");
//            }
//            System.out.println();
//        }
//        System.out.println(distances[0][3]);
        
        OUTER:
        while (true) {
            System.out.println("1) Ruta mas corta entre dos ciudades");
            System.out.println("2) Centro del grafo");
            System.out.println("3) Realizar modificaciones");
            System.out.println("4) Salir");
            
            int seleccion1;
            seleccion1 = entrada.nextInt();
            
            switch (seleccion1) {
                case 1:
                    System.out.println("Ingrese la ciudad de origen");
                    city1 = entrada.nextLine();
                    city1 = entrada.nextLine();
                    System.out.println("Ingrese la ciudad de destino");
                    city2 = entrada.nextLine();
                    
                    int i = ciudades.indexOf(city1);
                    int j = ciudades.indexOf(city2);
                    System.out.println("La distancia mas corta entre ambas ciudades es: "
                            +distances[i][j]+" KM");
                    System.out.println("Las ciudades que recorre son: ");
                    
                    // A partir de aqui probe hacer que se imprimiera el recorrido de las ciudades
                    // Pero no lo logre
                    System.out.println(ciudades.get(i));
                    
                    //La matriz recorrido la cree para inventarme una manera de guardar
                    //el recorrido optimo
                    
                    String[][] recorrido;
                    recorrido = f.matrizRecorrido();
                    String texto;
                    texto = recorrido[i][j];
                   
                    // Corto el texto predeterminado en cada entrada de la matriz recorrido
                    // este texto predeterminado puse que fuera infty 
                    // para ello, ver clase floyd warshall
                    
                    texto= texto.substring(5,texto.length());
                    int integer;
                    double total=0;
                    integer= j;
                    for (int k=texto.length()-1; k>= 0; k--){
                        // este fue mi intento para que se guardara el recorrido
                        // y que imprimiera las ciudades por las que atraviesa
                        // pero no funciona
                        
                        if (grafo[integer][Character.getNumericValue(texto.charAt(k))]<= distances[i][j]){
                            integer=Character.getNumericValue(texto.charAt(k));
                            total=total+grafo[integer][Character.getNumericValue(texto.charAt(k))];
                            System.out.println(ciudades.get(Character.getNumericValue(texto.charAt(k))));
                        }
                        if (total==distances[i][j])
                            break;
                    }
                    break;
                case 2:
                    System.out.println("La ciudad del centro del Grafo es:");
                    
                    break;
                case 3:
                    String seleccion2 = "";
                    System.out.println("a) Interrupcion de trafico entre par de ciudades");
                    System.out.println("b) Establecer conexion entre dos ciudades");
                    seleccion2 = entrada.next();
                    
                    
                    if (seleccion2.equalsIgnoreCase("a")){
                         System.out.println("Ingrese la primer ciudad: ");
                        String ciudadA=scanner.next();
                        System.out.println("Ingrese la segunda ciudad: ");
                        String ciudadB=scanner.next();
                        while(scanner.hasNextLine()){
                            String lectura=br1.readLine();
                            if((lectura.contains(ciudadA)) && (lectura.contains(ciudadB))){
                                
                            }
                            
                        }
                        
                        
                        
                    }
                    if (seleccion2.equalsIgnoreCase("b")){
                        // ejecutar establecer conexion.
                        System.out.println("Ingrese la primer ciudad: ");
                        String ciudadA=scanner.next();
                        System.out.println("Ingrese la segunda ciudad: ");
                        String ciudadB=scanner.next();
                        System.out.println("Ingrese *unicamente la magnitud* de distancia entre ambas ciudades.");
                        String distanciaCiudades=scanner.next();
                        
                        //Escritura al archivo..
                        writer.write(ciudadA+""+ciudadB+""+distanciaCiudades);
                        writer.close();
                        
                        
                    }
                    
                    break;
                case 4:
                    break OUTER;
                default:
                    break;
            }
            System.out.println(" ");
        }
    }
}