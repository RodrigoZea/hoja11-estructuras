"""
UVG
Algoritmos y Estructuras de Datos
Seccion 20

Integrantes del equipo:
Rodrigo Urrutia - 16139
Francisco Molina - 17050
Rodrigo Zea - 17058

"""

from ht11operations import *

# Variable looper
j = True

# Creamos el grafo antes de operar con el
createGraph()

options = {'1': shortRoute, '2': centerGraph, '3': trafficCities, '4': linkCities, '5': exitProgram}

while (j):
    printMenu()
    opt = input("Ingrese la opcion que desea tomar: ")
    options[opt]()
