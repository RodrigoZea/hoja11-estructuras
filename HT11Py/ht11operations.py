"""
UVG
Algoritmos y Estructuras de Datos
Seccion 20

Integrantes del equipo:
Rodrigo Urrutia - 16139
Francisco Molina - 17050
Rodrigo Zea - 17058

"""

import networkx as nx
import sys

# Creacion del grafo
graph = nx.Graph()

# Despliega el menu principal
def printMenu():
    print("Bienvenido al programa, seleccione su opcion: ")
    print("1. Ruta mas corta entre dos ciudades")
    print("2. Ciudad en el centro del grafo ")
    print("3. Agregar interrupcion de trafico entre dos ciudades")
    print("4. Establecer conexion entre ciudades")
    print("5. Finalizar el programa")

# Lee el archivo de texto y a partir de eso genera el grafo
def createGraph():
    # Lectura de archivo texto
    guateF = open("guategrafo.txt", "r+")

    # Se recorre el archivo de texto, cada linea
    for line in guateF:
        linea = line.split(" ")
        city1 = linea[0]
        city2 = linea[1]
        distance = float(linea[2])
        # Se agregan los vertices
        graph.add_edge(city1, city2, weight=distance)
    # Se termina de leer el archivo de texto
    guateF.close()
    
# Provee la ruta mas corta entre dos ciudades
def shortRoute():
    print(" ----- ")
    cityList = []
    nodes = graph.nodes()
    checker = True

    # Se pide ciudad de inicio
    cityStart = input("Ingrese la ciudad de origen: ")
    # Validacion
    while cityStart not in nodes:
        cityStart = input("Ingrese una ciudad valida: ")
    # Se pide ciudad de destino
    cityEnd = input("Ingrese la ciudad de destino: ")
    # Validacion
    while cityEnd not in nodes:
        cityEnd = input("Ingrese una ciudad valida: ")
    # Se obtienen los caminos del grafo
    path = nx.floyd_warshall_predecessor_and_distance(graph)

    try:
        # Ciudad que conecta a las otras ciudades
        cityLink = path[0][cityStart][cityEnd]
        distance = path[1][cityStart][cityEnd]
        cityList.append(cityLink)

        while checker:
            if (cityLink != cityStart):
                cityLink1 = path[0][cityStart][cityLink]
                cityList.append(cityLink1)
                cityLink = cityLink1
            else:
                checker = False

        cityList.reverse()

        print("La distancia mas corta entre ambas ciudades es:")
        print(", ".join(cityList),",",cityEnd)
        print("La distancia total es de ", distance, "km")
        print(" ----- ")
        
    except KeyError:
        print ("Error, no existe relacion entre ciudades, pruebe con otras")

    return
# Provee la ciudad que esta en el centro
def centerGraph():
    print(" ----- ") 
    # Se instancia el grafo de nuevo en caso que se haya modificado
    graph = nx.Graph()

    centerCity = nx.center(graph)
    print("Ciudades en el centro: "+"\n".join(centerCity))
    print(" ----- ") 
    return
# Agrega trafico entre ciudades
def trafficCities():
    nodes = graph.nodes()    
    print(" ----- ")    
    paths = nx.floyd_warshall_predecessor_and_distance(graph)
    
    # Se pide ciudad de inicio
    cityStart = input("Ingrese la ciudad de origen: ")
    # Validacion
    while cityStart not in nodes:
        cityStart = input("Ingrese una ciudad valida: ")
    # Se pide ciudad de destino
    cityEnd = input("Ingrese la ciudad de destino: ")
    # Validacion
    while cityEnd not in nodes:
        cityEnd = input("Ingrese una ciudad valida: ")

    delay = input("Agregue mas distancia para simular trafico: ")
        
    actualDis = paths[1][cityStart][cityEnd]

    if (actualDis != 0):
            graph[cityStart][cityEnd]['weight'] = int(delay) + actualDis
    print(" ----- ")    
    return
# Realiza una conexion entre ciudades
def linkCities():
    print(" ----- ")
    # Se pide ciudad de inicio
    cityStart = input("Ingrese la ciudad de origen: ")

    # Se pide ciudad de destino
    cityEnd = input("Ingrese la ciudad de destino: ")

    distance = input("Ingrese una distancia entre las dos ciudades: ")

    file = open("guategrafo.txt", "r+")
    lines = file.readlines()
    file.write("\n"+cityStart+" "+cityEnd+" "+str(distance))
    file.close

    createGraph()

    print("Se actualizaron los datos")
    print(" ----- ")
        
    return
# Sale del programa
def exitProgram():
    print ("Ha salido del programa")
    sys.exit(0)
